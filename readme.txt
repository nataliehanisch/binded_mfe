Requirements

- Python 2.7
    - There is a bug in 2.6 which prevents it from working. 
		(http://bugs.python.org/issue10806)
- ViennaRNA 2.x


Data structure: a 'bind site' is a string delimited by a hyphen.  
	Example: '6-34'
	The above denotes a bind site that starts at position 6 and
	ends at position 34. It is zero based and is equivalent to 
	the set of integers from [6, 34) in mathmatical notation.
	
	Bind Site files must contain one bind site per line, with the bind
	site as the last item per line. If there are other items on a 
	line, they must be separated from the bind site with a space.
	Example line: 'let-7/98/4458/4500: seed locus = 7 13'
	Example line: '7 13'
	The single quotes are not included.
	
	Sequence files contain a sequence as the 4th item in a
	tab-delimited file (where counting starts at 0).


Notes on the vienna RNA python; currently crashing when in use.
http://www.tbi.univie.ac.at/RNA/ViennaRNA/doc/html/group__mfe__fold.html#gadb973133c241d57c04b253df35e4d34e
mfeData2 = RNA.fold_par(line, constraint, None,True,False)