# Overview

Simulations of RNA folding with the [Vienna RNA Package](https://www.tbi.univie.ac.at/RNA/index.html) in order to observe RNA-protein cooperativity mediated through the RNA molecules as a mechanism for gene silencing. 
