import unittest
from mock import patch, MagicMock
from bindedFE import (getSingleMFE, getSinglePFE, getBindSitesFromFile, 
	getBindMFElist, getConstraintString, getDelDelGList, 
	createDoubleBindMFEMatrix)


class Test_GetSingleMFE(unittest.TestCase):
	
	test_sequence   = 'CAUCUGCUACCUCCUCCCCCCUCCUCUGAAACAGCUGCCUUAGCUUCAGGAACCUCGAGUACUGUGGGCAAUUUAGAAAAAGAACAUGCAGUUUGAAAUUCUGAAUUUGCAAAGUACUGUAAGAAUAAUUUAUAGUAAUGAGUUUAAAAAUCAACUUUUUAUUGCCUUCUCACCAGCUGCAAAGUGUUUUGUACCAGUGAAUUUUUGCAAUAAUGCAGUAUGGUACAUUUUUCAACUUUGAAUAAAGAAUACUUGAACUUGUCCUUGUUGAAUC'
	test_constraint = '..xxxxxxxxxx...................xx........................................xxx......................................................................................................................................................................................................'
	
	def test_with_no_constraint(self):
		
		result = getSingleMFE(self.test_sequence)
		
		self.assertEqual(result, '-49.60')
		
	def test_constrained(self):
	
		result = getSingleMFE(self.test_sequence, self.test_constraint)
		
		self.assertEqual(result, '-46.40')
		
class Test_GetSinglePFE(unittest.TestCase):
	
	@patch('bindedFE.Popen')
	def test_when_RNAfold_output_has_free_energy_line(self, mock_Popen):
		
		self.wire_RNAfold_to_return(
			self.mock_RNAfold_output_w_free_energy, mock_Popen)
			
		# Act
		result = getSinglePFE('gatc', '...x')
		
		# Assert
		self.RNAfold_proc.communicate \
			.assert_called_with(input='gatc\n...x')
			
		self.assertEqual(result, self.expected_result_w_free_energy)
	
	@patch('bindedFE.Popen')
	def test_when_RNAfold_output_wo_free_energy_line(self, mock_Popen):
		
		self.wire_RNAfold_to_return(
			self.mock_RNAfold_output_wo_free_energy, mock_Popen)
		
		# Act
		result = getSinglePFE('whateverGATC')
 
		self.assertEqual(result, self.expected_result_wo_free_energy)
	
	def wire_RNAfold_to_return(self, string, mock_Popen):
		""" Given the MagicMock for Popen, it will wire the process
			to return the given string. """
		# Have Popen return a fake RNAfold process
		mock_Popen.return_value = self.RNAfold_proc = MagicMock()
		
		# Have the fake RNAfold process return mocked data
		self.RNAfold_proc.communicate = MagicMock()
		self.RNAfold_proc.communicate.return_value = (string, None)
	
	# # # MOCK DATA FOR RNAfold
	
	# Case: output with 'free energy' report on the third line
	mock_RNAfold_output_w_free_energy = \
"""GCCCACUGGAGGCGCCGCCCCACCCGGCCCACUGGCAGACACAGACCCAGGCAGCACCAGGCCCCAGCUCCCUCCGGGGGCCCUCCAGGAACCACCAAGCUCUCUCACGACCUUCCCAAUCUUCCAGAAAGCUUGGUCCGCAGAAGCCCUGCCUGGUCCAGUCCGGGGGCGGCCAGGCCAACUGCAAGAUUCUGGACUGUUUUGGUGGCAUCCAAAGACGAUCUCAGAGCACUUUGAACCUCUCUGUUGAGUUUUCUUUUUGUUCCCCACCCUUCACUUGCUUCACUGUUUUUUUUUUUUUCGUUUUUUUUUUUUAAGUUUGUUUUGGAAAUAACAGAAAAGAUAUUUAUUGGCCAGGAAGUUGGUGCUGCUAAGACCGAGAAAUUUAAAGAAUCGGACAGAUGGACGCAGAGAACCAGAGGGCAAUGUGGGACCUUCUCUUGCCAUGGCCUCAGGUCUUGAGGGAAGGCUCGGGCCUAGGUUUCUGGACUGCAAAGGGGACCCCCAGGUGGGAGGGGCAGGAAGCAGCCGGAGUGAGCCCUUCGCCCCUGAGUGCCUGGCUCGCACCUCUCGAGCUGUGCCCUGGGCAUCACUGAGCAGAGGGGUGCGGCAGCUUCAGGAGGCAGAAACGAGAGGGGUGGGAGAUCCACAGGACCAAAGGGUGCAGUGAUGGGCUGGGUGGCGGGCCCAGGGAAAUGGGGUGGGGAUGGGCUGGGUAAGACCUGGCCCUCCCCUGCUGCCCUGAAGACCACCCCAGUCUACCUCGGUGCUGGCCAGGAAACCUUUCGGCUACCUCUCCCACCAUCCCAGACUGUGGGCAGGGGGAUGGGGAGGGAGUGGGCCUGGAUCUGGGACCUCCCUCCAGAAUUUCCUCCAGAUGGGGGCAGUGCCCAGGGAGGGGUUAUUUGUCUUCCCUGCCAUGGAGUGGGAAUCCCCAGCCGAGGCCCUAGGCCCCUCAGCAGGGAAGGGAUCCCCGGGGAGGCAGGUCCCAGGAGCAGACCCUGCCCCCAGCCCCCACAGACACACCCCAAUCUGAAAGCCAUGCGUGUCGGUGUAUAUAGGAACCAUGUACAGAGCCCAGAGAAGCCCCCUACAUCCCCCCGGGAAAAAAAAGAAAACUAGACAGAAACUCAUCUAUAUAUUCUGUAUCUGGAGUUCCGUUUUGAAUAUUAACUGUGUUAUUUUUAUACACUUUUUAAGCCUUAACUCGCCAUUGAUUUACCAGUUUAACGUUUCCUGGGGUUUCUUUGCCCAUGGGGUUCUCUGCCCCCACCCCCGGCCCUUUGUUUGACUUGCGUCGUCUGAUACUCAGUAUUGUAGCUUUUUGUCCGCAUGUUACUCCCUGUAAAUACGCUGUUAUACAUACUGUUAACACCCCUUUGCUUUUUCUAUGGGACCUCCAGGCCACCAUAUUUAGAACUAGUUACCUUAUUAAAAAAGAAAAAACAGUCUGUUGGCUUCUCAGUCUGCAUCUUGGAGGCAGGGAGGUGAGGGCAGGUGCCCCUCAGACACUUCAGGAAGGUAGUUUGCAUUCUAUUUAAAAAAGGGAGUGGGGAGCAAAUGAAAAUCAAAUGUGGGGGGAAAACACUAAAGGGGGCAAGAAACAAAGGAAUUACAAACCCUCUGCUCUUUGUAUUUCUCUGUUGUGAAGAAUAAACUGUACCUGCACCCGG
.....((((..((((((((((...(((...(((((...........((((((((.....((((((...........)))))).....(((....(((((((.(((....................))).))))))))))((....)).)))))))).))))))))))))))))(((.....)))((((.(((((((.((((((((......)).)))))).)).)))))...))))..((((((..((((.(((((..((((((((((((((((((((((((..(((((((((((((((((.((((((((((((((((.....)))))))).....)))))))).......((.((((((((((((((((......((((.............)))).((((((.((((((.((((.(((((((.....(((.((((((((...((.((((...)))).)))))))))))))((((......((((((.((((.....(..(((((....))).))..).....))))))))))...))))((((((((((((.((...((.((((((((((..((((((((...))))).....))).)))))))))))).)))))))).))).)))......((((((((.(...(((.(.(((....))).).)))..((((((((.((((((((((......))))..(((((.(((((((((...))))))))))))))..((((((((..((((......((((.((((((.........(((..((((((.(((.(((.((((.((..((((.....))))..)))))).)))(((((.((((((((.(..(((((...((((((((......(((((....))))).......(((((((((........)))))))))...))))..))))...))))).).))))))...))))))))))..))))))..))))))))))))).)))).)))).))))..))))))))))))))...((((...((((((..((((..((...))...))))(((((((.......))))))).....((((((((((((.....((((...)))).......(((..((((((((((.............)))))..)))))..)))((..((((...((((..((((.........))))...))))...))))..))..((((......))))............))))))))))))....))))))..))))).))))))))..)))))))))))..).))))))))))).....))))))..)))))))))).))((.((((((.....)).)))).))(((((.(((...)))))))).......(((..(((((((((.((....)).).)))....)))))..))).........)))))))))))))))))...((.(((((((.((((.((...)).)))).))))))).)))))))))).....(((((((((.....)))).)))))...............)))).)))))))))))).)))))))))....))))))...........(((((......................)))))(((..(((((.((((........)))).)))))..)))...))..)))) (-611.00)
free energy =  -644.35
..,..((((.,((((((((((..{(((...,||||,.{{{...{(,((((((((.....((((((.{.........)))))).....((,...{(((((((.(((...({............,,.))).)))))))))){,....)}.)))))))))),,))),}))))))))(((.....))){(((.(((((((.((((((((......))))))))).)).)))))...})}}..((((((.,(({{,(((((..((((((((((((((((((((((((,.(((((((((((((((((.,{,,,{{{{{{{{{{{,,,,,|||},}},.,,}})))}}}},......,((.((((((((((((((((......((((.............)))).((((({{((((((.(((,{(((((((.....(((.(((((((({,.{,.((((...)))).,))))))))))))((((.{....((((((.((((.....(..(((((....))).))..).....)))))))))),,.))))((((((((((((.((.,.((.((((((((((..((((((((...))))).....))).)))))))))))),)))))))).))).))}......((((((((.{.{{(,,.|}|{{...{|||{|.|{|..((((((((.((((((((((......)))),,(((((.(((((((((...))))))))))))))..({(({{{{..{{{{.,,||,{{{{.({{,||,|,|{{((,(({..,|}}}|))|||((((((((.(({((((,..{{{,,,,}}),))))}))||{|||||,||{{({{,.,{{((({{|{|{{|||.....,,(((((...,|||||{.,.|}}|{|||||||}.......|||||||||{..{((..((((....)))).))}.|||{,..})))}}.,.))}})))))),}.,,)}}}))))),,)))),)))).}))).,)))))))))))))).}}}.|,..}}}}}....((({..((...}},,.))))(((((((.{....,))))))).....((((((((((((...,,.,,({{{{||{{{.....(((..((((((((((.............))))),.}))))..))).,,,|,,,...,,{{..((((.,.....},))))...||||{{..|||.,|}..,{{|,.....}})),,,...}))))))))),,,,,,)).....))))))))))),.)))))))),.)))))))))))..}}))))))))))).....)))))),.}))))))))).)){{.{{((((.....)))},}}.},(((((.{({...})}))))).......(((..(((((((({.((....)).).)))}...,})))..))}.........)))))))))))))))))...,{.(((((((.((((.({...}).)))).))))))).)}))))))))..,..((((({({{.....}))}.)))))...............)))).)))))))))))).)))))))}),,,.))))))......,...|{((|{|,.,..,{{.{((,,..,,,,..},}.}|,..}}}}},|||}},},,,||}.||.,,,....,,.,.,))..)))) [-644.35]
.....((((..((((((((((................(((...((.((((((((.....((((((...........)))))).....((....((((((((.(((....................))).)))))))))).........))))))))))..)))..))))))))(((.....)))(((..(((((((.((((((((......)))).)))).)).)))))....)))..((((((..((((.(((((..((((((((((((((((((((((((..(((((((((((((((((..................................................((.(((((((((.((((((......((((.............)))).(((((..((((((.(((.((((((((.....(((.(((((((((.....((((...))))..))))))))))))((((......((((((.((((.....(..(((((....))).))..).....))))))))))...))))((((((((((((.((...((.((((((((((..((((((((...))))).....))).)))))))))))).)))))))).))).)))......((((((((.................(((((..(((..((((((((.((((((((((......))))..(((((.(((((((((...))))))))))))))..((((((((..((........................(((............)))...........(((((((...(((....))).)))))))................................................................................(((((((((...((..((((....)))).))..((((...)))).....)))))))))...................)).)))).))))..)))))))))))))).))).....)))))....((((............))))(((((((.......))))))).....(((((...............((((((((((.....(((..((((.(((((.............)))))...))))..))).................((((.........))))........................................))))))))))...................)))))..))))))))..)))))))))))..).))))).))))).....))))))...))))))))).))......(((.....))).......(((((.((.....))))))).......(((..((((.(((..((....))...))).....))))..))).........)))))))))))))))))....(.(((((((.((((.((...)).)))).))))))).).)))))))).....(((((((((.....)))).)))))...............)))).)))))))))))).)))))))))....))))))....................................................................................))..)))) {-485.47 d=281.54}
 frequency of mfe structure in ensemble 3.17971e-24; ensemble diversity 426.44"""
	expected_result_w_free_energy = '-644.35'
	
	# Case: output without 'free energy' report on the third line
	mock_RNAfold_output_wo_free_energy = \
"""GCGUCUACAGACAGCUCACCAUUUUUGUCCUGUAUCUGUAAACACUUUUUGUUCUUAGUCUUUUUCUUGUAAAAUUGAUGUUCUUUAAAAUCGUUAAUGUAUAACAGGGCUUAUGUUUCAGUUUGUUUUCCGUUCUGUUUUAAACAGAAAAUAAAAGGAGUGUAAGCUCCUUUUCUCAUUUCAAAGUUGCUACCAGUGUAUGCAGUAAUUAGAACAAAGAAGAAACAUUCAGUAGAACAUUUUAUUGCCUAGUUGACAACAUUGCUUGAAUGCUGGUGGUUCCUAUCCCUUUGACACUACACAAUUUUCUAAUAUGUGUUAAUGCUAUGUGACAAAACGCCCUGAUUCCUAGUGCCAAAGGUUCAACUUAAUGUAUAUACCUGAAAACCCAUGCAUUUGUGCUCUUUUUUUUUUUUUAUGGUGCUUGAAGUAAAACAGCCCAUCCUCUGCAAGUCCAUCUAUGUUGUUCUUAGGCAUUCUAUCUUUGCUCAAAUUGUUGAAGGAUGGUGAUUUGUUUCAUGGUUUUUGUAUUUGAGUCUAAUGCACGUUCUAACAUGAUAGAGGCAAUGCAUUAUUGUGUAGCCACGGUUUUCUGGAAAAGUUGAUAUUUUAGGAAUUGUAUUUCAGAUCUUAAAUAAAAUUUGUUUCUAAAUUUCAAAGCAA
((((.(((((((((..((.......))..)))..)))))).)).............((((((....(((((..(((((((...........))))))).))))).)))))).(((((((..((((((((...(((((((...)))))))...(((((((((....)))))))))............(((((((....))).)))).....))))))))...)))))))...((((((........(((((((..((((((((.((..((((((..(.((((......((((((.(((((...((((..........(((((........))))).........))))..))))).))))))((((................)))))))))..))))))..))...............((((.(((((.((................)).)))))))))..)))))))).)))))))))))))....((((((((.....((((((.((((......)))).))))))..))))))))..((((((..(((((......)))))....))))))((((((....))))))....((((((.........(((((((.(((.....))).))))))).........)))))).........)).. (-137.90)
{{{{.(((((((((..((.......))..))),.,))))).||,,...,{{|{...{{{{{,....(((((..(((((((...........))))))).})))),}))|||.(((((((..((((((({...(((((({...)))))))...{((((((((....))))))))}..........,{((((({({{..|,,,}|}}},}|||))))))}...}}}}}}}..,({((({........(((((((..((((((((.((.,((((((..(.((((......((((((.((((((((((((....,,,.})))|,,.......{{|.,,...}}}...},,...})))).)))))){{{{,,.....,,.......})}}}))))..)))))),.))...............((((.(((((.{(................)}.)))))))))..)))))))).)))))))}}}},},,..{(((((((.....((((((.((((......)))).))))))..)))))))).,||{{{{..{((({....,.))))|||..|})}}}},||||},}})}}}},,..,||||||||{{,,.,,{((((((,({{.....})).}})})))}},|||,||}}}}))}},.,,,,.)).. [-150.69]
..((.(((((.(((..((.......))..)))...))))).))........................((((..(((((((...........))))))).)))).........(((((((..(((((((....(((((((...)))))))...(((((((((....))))))))).....................................)))))))...))))))).................(((((((..((((((((.((..((((((..(.((((......((((((.(((((((((...........))))...............................))))).)))))).((..................)).)))))..))))))..))...............((((.(((((.((................)).)))))))))..)))))))).)))))))..........((((((((.....((((((.((((......)))).))))))..))))))))...........((((......))))................................................(((((..((.....))..))))).............................. {-96.67 d=118.46}
 frequency of mfe structure in ensemble 9.77614e-10; ensemble diversity 185.25"""
	expected_result_wo_free_energy = '-150.69'
	# # # END MOCK DATA
		
class Test_GetBindSitesFromFile(unittest.TestCase):
	
	test_file = 'test_data/EZH2.bnd'
	
	def test_bind_sites_getter(self):
		expected_bind_sites = ['7-13', '36-42', '59-65', '114-121', '115-121', '171-177', '207-213', '250-257']
		
		result = getBindSitesFromFile(self.test_file)
		
		self.assertEqual(result, expected_bind_sites)

from bindedFE import getSequenceFromFile

class Test_GetSequenceFromFile(unittest.TestCase):
		
		expected_seq = 'CAUCUGCUACCUCCUCCCCCCUCCUCUGAAACAGCUGCCUUAGCUUCAGGAACCUCGAGUACUGUGGGCAAUUUAGAAAAAGAACAUGCAGUUUGAAAUUCUGAAUUUGCAAAGUACUGUAAGAAUAAUUUAUAGUAAUGAGUUUAAAAAUCAACUUUUUAUUGCCUUCUCACCAGCUGCAAAGUGUUUUGUACCAGUGAAUUUUUGCAAUAAUGCAGUAUGGUACAUUUUUCAACUUUGAAUAAAGAAUACUUGAACUUGUCCUUGUUGAAUC'
		
		def test_sequence_string_obtained(self):
			
			result = getSequenceFromFile('test_data/EZH2.seq')
			
			self.assertEqual(result, self.expected_seq)
			
		def test_sequence_string_obtained_with_complicated_file(self):
			
			result = getSequenceFromFile('test_data/EZH2_bad.seq')
			
			self.assertEqual(result, self.expected_seq)
			
###

class Test_GetBindMFEList(unittest.TestCase):
	
	expectedBindMFElist = ['-55.73', '-53.69', '-54.51', '-51.74', '-51.84', '-54.40', '-52.49', '-55.29']
	test_sequence = 'CAUCUGCUACCUCCUCCCCCCUCCUCUGAAACAGCUGCCUUAGCUUCAGGAACCUCGAGUACUGUGGGCAAUUUAGAAAAAGAACAUGCAGUUUGAAAUUCUGAAUUUGCAAAGUACUGUAAGAAUAAUUUAUAGUAAUGAGUUUAAAAAUCAACUUUUUAUUGCCUUCUCACCAGCUGCAAAGUGUUUUGUACCAGUGAAUUUUUGCAAUAAUGCAGUAUGGUACAUUUUUCAACUUUGAAUAAAGAAUACUUGAACUUGUCCUUGUUGAAUC'
	test_bind_sites_list = ['7-13', '36-42', '59-65', '114-121', '115-121', '171-177', '207-213', '250-257']
	
	def test_bind_MFE_list(self):
		
		result = getBindMFElist(self.test_sequence, self.test_bind_sites_list)
		
		self.assertEqual(result, self.expectedBindMFElist)
		
from bindedFE import replaceCharsWithDot
class Test_GetConstraintString(unittest.TestCase):
	
	expectedConstraint 	= '.......xxxxxx.....................................................................................................................................................................................................................................................................'
	test_sequence 		= 'CAUCUGCUACCUCCUCCCCCCUCCUCUGAAACAGCUGCCUUAGCUUCAGGAACCUCGAGUACUGUGGGCAAUUUAGAAAAAGAACAUGCAGUUUGAAAUUCUGAAUUUGCAAAGUACUGUAAGAAUAAUUUAUAGUAAUGAGUUUAAAAAUCAACUUUUUAUUGCCUUCUCACCAGCUGCAAAGUGUUUUGUACCAGUGAAUUUUUGCAAUAAUGCAGUAUGGUACAUUUUUCAACUUUGAAUAAAGAAUACUUGAACUUGUCCUUGUUGAAUC'
	test_bindSiteList	= ['7-13'] 
	
	
	def test_with_one_bind_site(self):
		
		result = getConstraintString(self.test_sequence, self.test_bindSiteList)
		
		self.assertEqual(result, self.expectedConstraint)
		
	def test_with_two_bind_sites(self):
		
		expectedConstraint = '.......xxxxxx.......xxxxx.........................................................................................................................................................................................................................................................'
		test_bindSiteList	= ['7-13', '20-25'] 
		
		result = getConstraintString(self.test_sequence, test_bindSiteList)
		
		self.assertEqual(result, expectedConstraint)
	
	def test_with_three_bind_sites(self):
		expectedConstraint = '.......xxxxxx.......xxxxx....................................................................................................................................................xxxxxxxxx............................................................................................'
		test_bindSiteList	= ['7-13', '20-25', '173-182'] 
		
		result = getConstraintString(self.test_sequence, test_bindSiteList)
		
		self.assertEqual(result, expectedConstraint)
		
	def test_replaceCharsWithDot(self):
		
		result = replaceCharsWithDot('GAAAATCFG')
		
		self.assertEqual(result, '.........')

from bindedFE import getSingleBindDeltaGList

class test_GetDeltaG(unittest.TestCase):
	
	@patch('bindedFE.getSinglePFE')
	@patch('bindedFE.getBindMFElist')
	def test_with_one_bind_site(self, mock_getBindMFEList, mock_getSinglePFE):
		
		mock_getSinglePFE.return_value = '30'
		mock_getBindMFEList.return_value = ['50']
		
		result = getSingleBindDeltaGList('CAGCTCCAA', ['1-3'])
		
		self.assertEqual(result, [20])
		
	@patch('bindedFE.getSinglePFE')
	@patch('bindedFE.getBindMFElist')
	def test_with_two_bind_sites(self, mock_getBindMFEList, mock_getSinglePFE):
		
		mock_getSinglePFE.return_value = '30'
		mock_getBindMFEList.return_value = ['50.32', '-70.81']
		
		result = getSingleBindDeltaGList('CAGCTCCAA', ['1-3', '5-7'])
		
		self.assertEqual(result, [20.32, -100.81])

from bindedFE import getDeltaGSingleBindDataCollection
class test_getDeltaGSingleBindDataCollection(unittest.TestCase):
	
	@patch('bindedFE.getSingleBindDeltaGList')
	def test_expected_values(self, mock_getSingleBindDeltaGList):
		
		mock_getSingleBindDeltaGList.return_value = [30, 40]
		
		result = getDeltaGSingleBindDataCollection('GATCATTATTTA', ['1-2', '6-9'])
		
		self.assertEqual(result, [
			['1-2', 30],
			['6-9', 40]
		])

@unittest.skip("This test needs to be redesigned to not be so large and integrated.")
class test_doubleBindMatrix(unittest.TestCase):
	"""
	Currently an integration test.
	"""
	def test_stuff(self):
		
		
		testSequence = 'CAUCUGCUACCUCCUCCCCCCUCCUCUGAAACAGCUGCCUUAGCUUCAGGAACCUCGAGUACUGUGGGCAAUUUAGAAAAAGAACAUGCAGUUUGAAAUUCUGAAUUUGCAAAGUACUGUAAGAAUAAUUUAUAGUAAUGAGUUUAAAAAUCAACUUUUUAUUGCCUUCUCACCAGCUGCAAAGUGUUUUGUACCAGUGAAUUUUUGCAAUAAUGCAGUAUGGUACAUUUUUCAACUUUGAAUAAAGAAUACUUGAACUUGUCCUUGUUGAAUC'
		testBindSites = ['7-13', '36-42', '59-65', '114-121', '115-121', '171-177', '207-213', '250-257']
		expected_matrix = [
			['7-13', '36-42', '2.4'], 
			['7-13', '59-65', '1.0'], 
			['7-13', '114-121', '3.8'], 
			['7-13', '115-121', '3.8'], 
			['7-13', '171-177', '2.3'], 
			['7-13', '207-213', '3.3'], 
			['7-13', '250-257', '0.3'], 
			['36-42', '59-65', '3.2'], 
			['36-42', '114-121', '6.1'], 
			['36-42', '115-121', '6.1'], 
			['36-42', '171-177', '3.3'], 
			['36-42', '207-213', '5.5'], 
			['36-42', '250-257', '2.5'], 
			['59-65', '114-121', '5.4'], 
			['59-65', '115-121', '5.4'], 
			['59-65', '171-177', '3.9'], 
			['59-65', '207-213', '6.1'], 
			['59-65', '250-257', '1.1'], 
			['114-121', '115-121', 'X'], 
			['114-121', '171-177', '4.7'], 
			['114-121', '207-213', '7.6'], 
			['114-121', '250-257', '5.1'], 
			['115-121', '171-177', '4.7'], 
			['115-121', '207-213', '7.6'], 
			['115-121', '250-257', '5.1'], 
			['171-177', '207-213', '4.9'], 
			['171-177', '250-257', '3.4'], 
			['207-213', '250-257', '3.4']]
	
		result = createDoubleBindMFEMatrix(testSequence, testBindSites)
		
		self.assertEqual(result, expected_matrix)
		
		
class test_ddgList(unittest.TestCase):
	
	def test_that_deltaDeltaGList_is_produced_as_expected(self):
		
		deltaG_1_2 = [
			['7-13', '36-42', '2.4'], 
			['7-13', '41-65', '1.0'],
			['36-42', '41-65', 'X']
		]
		
		deltaG_1 = [
			['7-13', '5.1'],
			['36-42', '-2.8'],
			['41-65', '.9']
		]
		
		expected_result = [
			['7-13', '36-42', '0.1',23], 
			['7-13', '41-65', '-5.0',28],
			['36-42', '41-65', 'X',-1]
		] # deltaDeltaG
		
		result = getDelDelGList(deltaG_1_2, deltaG_1)
		
		self.assertEqual(result, expected_result)

from bindedFE import main
class MainTests(unittest.TestCase):
	
	def test_stuff(self):
		
		seq_file = 'test_data/EZH2.seq'
		bnd_file = 'test_data/EZH2.bnd'
		
		main(seq_file, bnd_file)
		
		


		