''' this is the mainfile '''

from subprocess import Popen, STDOUT, PIPE
import re

#### 
'''
Returns the starting location of a given bind site
'''
def getStartFromBindSite(bindSite) :
	start = bindSite.split('-')[0]
	return int(start)

'''
Returns the stop location for a given bind site
'''
def getStopFromBindSite(bindSite) :
	if bindSite == '99999--1' :
		return -1
	else :
		stop = bindSite.split('-')[1]
	return int(stop)

'''
Given a list of binding sites, returns them sorted by start location.
'''
def sortBindSites(bindList) :
	bindList.sort(key=getStartFromBindSite)
	return bindList

'''
Given a file name, this returns the bind sites in that file.
The file must contain one bind site per line, and the bind site
must be the last item on the line, separated from other items
with spaces. The bind site must be of the form '23-45'.
'''
def getBindSitesFromFile(filename):
	
	fileText = ((file(filename, 'r')).read()).split('\n')
	bindLocList = list()

	for line in range(len(fileText)) :
		if fileText[line] != '' :
			lineList = fileText[line].split()
			
			start = lineList[len(lineList) - 2]
			stop = lineList[len(lineList) - 1]
			
			bindLoc = start+'-'+stop
			bindLocList.append(bindLoc)
	bindLocList = sortBindSites(bindLocList)
		
	return bindLocList
	
def getSequenceFromFile(seq_path):
	
	with open(seq_path, 'r') as seq_file:
		seq_string = seq_file.read().split('\n')[0].split('\t')[4]
		seq_string = seq_string.strip('\n')
		
	return seq_string
###

'''
Given an RNA sequence this will return the MFE of that sequence.
Additionally, a constraint parameter can be given and will return
the MFE with that contraint
'''
def getSingleMFE(rnaSeq, constraint=None) :

	if constraint != None :
		line = rnaSeq + '\n' + constraint
		cmd = ['RNAfold', '-C', '--noPS']
		
	else :
		line = rnaSeq
		cmd = ['RNAfold', '--noPS']
	
	rnaFold = Popen(cmd, stdout=PIPE, stdin=PIPE, stderr=STDOUT)
		
	output = rnaFold.communicate(input=line)[0].split('\n')
	
	mfeData = output[1].split()[1].strip('(').strip(')')
	
	return mfeData

'''
Given an RNA sequence this will return the PFE (partitiion function
energy of that sequence. Additionally, a constraint parameter can be 
given and will return the MFE with that contraint
'''
def getSinglePFE(rnaSeq, constraint=None) :

	if constraint != None :
		line = rnaSeq + '\n' + constraint
		cmd = ['RNAfold', '-C', '--noPS', '-p']
		
	else :
		line = rnaSeq
		cmd = ['RNAfold', '--noPS', '-p']
	
	rnaFold = Popen(cmd, stdout=PIPE, stdin=PIPE, stderr=STDOUT)
	
	output = rnaFold.communicate(input=line)[0]
	
	# Find the decimal number within brackets
	mfeData = re.search('\[(-?[0-9]+\.[0-9]+)\]', output).groups(0)[0]
	
	return mfeData

'''
Given a sequence and a list of bind sites, returns a list containing
the MFE for a bind at each site.
Additionally, a list of lists of bindsites can be given, so that
several bind sites at once can be checked.
'''	
def getBindMFElist(seq, bindSites) :
	listOfMFEWithBind = list()
	
	for site in bindSites :
		constraint = getConstraintString(seq, [site]) #no this is fine, it's not all bind sites

		mfeWithBind = getSinglePFE(seq, constraint)
		
		listOfMFEWithBind.append(mfeWithBind)
	return listOfMFEWithBind

####
'''
Given a string, returns a string of the same length containing only
dots. Useful for building constraint strings.
'''
def replaceCharsWithDot(rnaSeq):
	
	dotString = ""
	
	for char in rnaSeq:
		dotString += '.'
		
	return dotString

'''
Given an RNA sequence and a list of binding sites, returns the
appropriate constraint string to use with Vienna RNA. It doesn't care
if the bind sites overlap.
'''
def getConstraintString(rnaSeq, bindSiteList):
	
	constraint = replaceCharsWithDot(rnaSeq)
	
	constraint = bytearray(constraint, 'ASCII')
	
	for site in bindSiteList:
		
		start = getStartFromBindSite(site)
		stop = getStopFromBindSite(site)
		
		for pos in range(start, stop):
			constraint[pos] = 'x'
	
	return str(constraint)

'''
Given a sequence string with corresponding bind sites, returns a list
of the energy difference required for each configuration to occur.
'''
def getSingleBindDeltaGList(sequenceStr, bindSites):
	
	mfeUnbinded = getSinglePFE(sequenceStr)
	listOfBindedMFE = getBindMFElist(sequenceStr, bindSites)
	
	deltaGlist = list()
	
	for bindE in listOfBindedMFE :
		deltaGlist.append(float(bindE)-float(mfeUnbinded))
		
	return deltaGlist

###
'''
Given a sequence string returns an embedded list containing each bind 
site and the corresponding change in energy required for the sequence 
to be unconstrained.
'''
def getDeltaGSingleBindDataCollection(sequenceStr, bindSites) :
	
	deltaGList = getSingleBindDeltaGList(sequenceStr, bindSites)
	
	matrix = list()
	for i in range(len(bindSites)) :
		matrix.append([bindSites[i], deltaGList[i]])
	return matrix
	
'''
Given a sequence string creates an embedded list where each row 
contains two binding sites and the resulting delta G energy difference 
for those sites being constrained open.
'''	
def createDoubleBindMFEMatrix(seqStr, bindSites) :
	
	s1 = 0 #loc of bind site 1
	s2 = 1 #loc of bind site 2
	
	matrix = list()
	baseMFE = getSinglePFE(seqStr)

	while (s1 < len(bindSites)) :
		while (s2 < len(bindSites)) :
			if getStopFromBindSite(bindSites[s1]) < getStartFromBindSite(bindSites[s2]) :
				constraint = getConstraintString(seqStr, [bindSites[s1], bindSites[s2]])
				deltaG = str(float(getSinglePFE(seqStr, constraint)) - float(baseMFE))
			else :
				deltaG = "X"
			matrix.append([bindSites[s1], bindSites[s2], deltaG])
			s2 += 1
		s1 += 1	
		s2 = s1 + 1
	
	return matrix
###

'''
Given a bind site and a 2-collumn list of the delta G for single
bind sites, returns the delta G energy difference for that site
'''
def findSiteDeltaG(site, deltaGList):
	
	""" Find the energy in the deltaGList for a particular bind site """
	
	for row in deltaGList :
		if row[0] == site :
			site_energy = row[1]
			return site_energy
	
'''
Given a g12 list and a g1 list, makes a matrix where the first two
columns are the sites, and the third is the delta delta G energy
difference based on those two sites.
'''
def getDelDelGList(g1_2, deltaGList) :
	
	delDelGList = list()
	
	for row in g1_2:
		s1 = row[0]
		s2 = row[1]
		e1_2 = row[2]
		
		if e1_2 == 'X' :
			delDelG = 'X'
		else :
			e1 = float(findSiteDeltaG(s1, deltaGList))
			e2 = float(findSiteDeltaG(s2, deltaGList))
			
			delDelG = str(round(float(e1_2) - e1 - e2, 3))
			
		delDelGList.append([s1, s2, delDelG, getStartFromBindSite(s2)-getStopFromBindSite(s1)])
	
	return delDelGList
###

'''
given a matrix, turns it into a space-delimited string to save to file
'''
def matrixToString(matrix) :
	string = ""
	for row in matrix :
		for item in row :
			string += str(item) + " " #make sure item is string
		string += "\n" #new line for each row
	return string

'''
The main method. Takes a file path for a sequence and for a list of 
binding sites. Using this information it creates a list to contain 
the bind sites and a string to reference the sequence. Then a list
is created where the first and second columns are the related bind
sites and the third column has the associated delta delta G energy
difference. 
'''
def main(seq_path, bnd_path):
	
	seq_string = getSequenceFromFile(seq_path)
	bind_sites = getBindSitesFromFile(bnd_path)
		
	singleBindDeltaGList = getDeltaGSingleBindDataCollection(seq_string, bind_sites)
	
	g_1_2 = createDoubleBindMFEMatrix(seq_string, bind_sites)
	
	print matrixToString(getDelDelGList(g_1_2, singleBindDeltaGList))
	
	
	return 0